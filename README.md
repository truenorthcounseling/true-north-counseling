Counseling services for children, teens, and their families in a fun and relaxing setting, using play, art, sand trays, and games.

Parenting is hard.
Growing up is hard.
Sometimes we just need some help navigating lifes ups and downs.
True North Counseling is about helping you and your child find their True North or their authentic self in a confusing world.
When a child or teen is struggling to find their way in the world, to show their gifts and believe in themselves, they can act out or become anxious or depressed.
Together we can help your child or teen learn the coping skills and resilience to move forward and chart their course.

Tampas premier counseling service.

Website: https://yourtruenorthcounseling.com/
